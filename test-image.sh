#!/bin/bash
set -euo pipefail

# 1. Fetch and setup test framework.
# For more info and usage examples, see:
#   https://github.com/GoogleContainerTools/container-structure-test

# only linux and osx are supported
if [[ "$OSTYPE" == "linux-gnu" ]]; then
  cst_ostype="linux"
  docker_slim_url="https://downloads.dockerslim.com/releases/1.25.3/dist_linux.tar.gz"
  dist_name="dist_linux"
elif [[ "$OSTYPE" == "darwin"* ]]; then
  cst_ostype="darwin"
  docker_slim_url="https://downloads.dockerslim.com/releases/1.25.3/dist_mac.zip"
  dist_name="dist_mac"
else
  echo "Unsupported operating system[$OSTYPE]"
  exit 1
fi
mkdir -p build/tool
wget -N \
     -P build/tool \
      https://storage.googleapis.com/container-structure-test/latest/container-structure-test-${cst_ostype}-amd64
chmod +x build/tool/container-structure-test-${cst_ostype}-amd64

# 2. Build the docker image.
# You can set $1 to a Dockerfile to test interractively
docker build -f ${1-${DOCKER_FILE}} --tag amaretto-8 .

# 3. Test using container-structure-test.
build/tool/container-structure-test-${cst_ostype}-amd64 test \
    --image amaretto-8 \
    --config test-image.yaml

# 4. Build the docker image using Docker Slim
wget -N \
     -P build/tool \
     ${docker_slim_url}

if [[ "$OSTYPE" == "linux-gnu" ]]; then
    tar -xvf build/tool/dist_linux.tar.gz; rm build/tool/dist_linux.tar.gz
else 
    unzip -d build/tool build/tool/dist_mac.zip; rm build/tool/dist_mac.zip
fi

build/tool/${dist_name}/docker-slim build --http-probe=false --continue-after=1 amaretto-8

# 4. Test using container-structure-test.
build/tool/container-structure-test-${cst_ostype}-amd64 test \
    --image amaretto-8.slim \
    --config test-image.yaml

